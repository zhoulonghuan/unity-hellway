v0.2.0 (2017-11-26)
- Added: Menu, Paused & GameOver UI (also which version you're playing to the top right)
- Added: Health, Ammo and Reload UI
- Changed: Rebuilt weapon system and made it more robust
  - Shotguns and M1 Garand uses a different reload system
- Added: Weapons (most of them are now raycasting instead of shooting projectile):
  - Desert Eagle
  - Spas-12
  - AK-47
  - UMP-45 (now fixed)
  - M4A1
  - Milkor MGL (grenade launcher)
  - RPG-7
  - M1 Garand
- Added: Health and Ammo crates (powerup/pickups)
- Added: Attachments to most weapons, including: Laser Sight, Flashlight and Silencer
- Fixed: Explosive Barrels actually do damage now
- Fixed: Footsteps sound when moving
- Removed: Health bar above player


v0.1.1 (2017-10-24)
- Fixed: Standalone settings not showing up.


v0.1.0 (2017-10-24)
- Added: Basic movement and animations.
- Added: Fire weapon and ammo system.
- Added: Explosive barrel(s)!
- Added: Laser sight and flashlight (hotkeys: T and F).
