﻿using UnityEngine;
using UnityEngine.Events;

namespace Saucy.Hellway {
  public class GameManager : MonoBehaviour {
    [SerializeField] private GameObject playerPrefab = null;
    [SerializeField] private Transform spawnPoint = null;

    public UnityEvent OnPlayerSpawned;

    private void Start () {
      SpawnPlayer();
    }

    private void SpawnPlayer () {
      Instantiate(playerPrefab, spawnPoint.position, spawnPoint.rotation);

      OnPlayerSpawned?.Invoke();
    }
  }
}
