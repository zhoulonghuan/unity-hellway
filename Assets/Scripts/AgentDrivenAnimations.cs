﻿using UnityEngine;
using UnityEngine.AI;

namespace Saucy.Hellway {
  [RequireComponent(typeof(Animator))]
  [RequireComponent(typeof(NavMeshAgent))]
  public class AgentDrivenAnimations : MonoBehaviour {
    private Animator animator;
    private NavMeshAgent agent;

    private void Awake () {
      animator = GetComponent<Animator>();
      agent = GetComponent<NavMeshAgent>();
    }

    private void Update () {
      float _speed = agent.velocity.magnitude / agent.speed;

      animator.SetFloat("Speed", _speed, 0.1f, Time.deltaTime);
    }
  }
}
