﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace Saucy.Hellway {
  [RequireComponent(typeof(Rigidbody))]
  public class Ragdolls : MonoBehaviour {
    [SerializeField] private Collider characterCollider = null;
    // [SerializeField] private Animator animator = null;

    public UnityEvent OnEnableRagdoll;

    private Rigidbody rb;
    private List<Collider> ragdollParts = new List<Collider>();

    private void Awake () {
      rb = GetComponent<Rigidbody>();

      DisableRagdoll();
    }

    [Button]
    public void DisableRagdoll () {
      Collider[] _colliders = gameObject.GetComponentsInChildren<Collider>();

      foreach (Collider _collider in _colliders) {
        if (_collider.gameObject != gameObject) {
          _collider.isTrigger = true;
          ragdollParts.Add(_collider);
        }
      }
    }

    [Button]
    public void EnableRagdoll () {
      rb.useGravity = false;
      rb.velocity = Vector3.zero;
      characterCollider.enabled = false;
      // animator.enabled = false;
      OnEnableRagdoll?.Invoke();

      foreach (Collider _collider in ragdollParts) {
        if (_collider.gameObject != gameObject) {
          _collider.isTrigger = false;
          _collider.attachedRigidbody.velocity = Vector3.zero;
        }
      }
    }
  }
}
