﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Saucy.Data.RuntimeSets;

// TODO: Move movement code to another MovementInput script? Possible?

namespace Saucy.Hellway {
  [RequireComponent(typeof(NavMeshAgent))]
  public class EnemyController : MonoBehaviour {
    [SerializeField] private float lookRadius = 10f;
    [SerializeField] private GameObjectRuntimeSet playerSet = null;
    [SerializeField] private float rotatinSpeed = 5f;

    private NavMeshAgent agent;
    private Transform target;

    private void Awake () {
      agent = GetComponent<NavMeshAgent>();
    }

    private void Start () {
      if (playerSet.Items.Count > 0) {
        target = playerSet.Items[0].transform;
      }
    }

    private void Update () {
      if (target == null) {
        return;
      }

      float _distance = Vector3.Distance(target.position, transform.position);

      if (_distance <= lookRadius) {
        agent.SetDestination(target.position);

        if (_distance <= agent.stoppingDistance) {
          // attack the target
          Debug.Log($"attacking player...");
          FaceTarget();
        }
      }
    }

    private void OnDrawGizmosSelected () {
      Gizmos.color = Color.red;
      Gizmos.DrawWireSphere(transform.position, lookRadius);
    }

    private void FaceTarget () {
      Vector3 _direction = (target.position - transform.position).normalized;
      Quaternion _lookRotation = Quaternion.LookRotation(new Vector3(_direction.x, 0f, _direction.z));

      transform.rotation = Quaternion.Slerp(transform.rotation, _lookRotation, Time.deltaTime * rotatinSpeed);
    }

    public void SetTarget (Transform _target) {
      target = _target;
    }
  }
}
