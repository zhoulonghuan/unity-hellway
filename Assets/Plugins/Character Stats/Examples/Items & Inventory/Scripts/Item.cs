using UnityEngine;

namespace Kryz.CharacterStats.Examples {
  [CreateAssetMenu(menuName = "Plugins/Character Stats/Examples/Item")]
  public class Item : ScriptableObject {
    public string ItemName;
    public Sprite Icon;
  }
}
