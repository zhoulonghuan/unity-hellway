﻿using UnityEngine;
using UnityEngine.UI;

namespace Kryz.CharacterStats.Examples {
  public class PlayerTextUI : MonoBehaviour {
    [SerializeField] private Player player = null;
    [SerializeField] private Text speedText = null;
    [SerializeField] private Text jumpText = null;

    private void OnValidate () {
      if (player == null)
        player = FindObjectOfType<Player>();
    }

    private void Update () {
      speedText.text = player.MovementSpeed.Value.ToString();
      jumpText.text = player.JumpForce.Value.ToString();
    }
  }
}
