﻿using UnityEngine;
using UnityEngine.UI;

namespace Kryz.CharacterStats.Examples {
  public class PlayerBarUI : MonoBehaviour {
    [SerializeField] private Player player = null;
    [SerializeField] private Image speedImage = null;
    [SerializeField] private Image jumpImage = null;
    [SerializeField] private float speedminValue = 2;
    [SerializeField] private float jumpminValue = 2;
    [SerializeField] private float speedMaxValue = 14;
    [SerializeField] private float jumpMaxValue = 14;

    private float speedMaxSize;
    private float jumpMaxSize;

    private void OnValidate () {
      if (player == null)
        player = FindObjectOfType<Player>();
    }

    private void Awake () {
      speedMaxSize = speedImage.rectTransform.sizeDelta.y;
      jumpMaxSize = jumpImage.rectTransform.sizeDelta.y;
    }

    private void Update () {
      Vector3 speedSize = speedImage.rectTransform.sizeDelta;
      speedSize.y = (player.MovementSpeed.Value - speedminValue) / speedMaxValue * speedMaxSize;
      speedImage.rectTransform.sizeDelta = speedSize;

      Vector3 jumpSize = jumpImage.rectTransform.sizeDelta;
      jumpSize.y = (player.JumpForce.Value - jumpminValue) / jumpMaxValue * jumpMaxSize;
      jumpImage.rectTransform.sizeDelta = jumpSize;
    }
  }
}
