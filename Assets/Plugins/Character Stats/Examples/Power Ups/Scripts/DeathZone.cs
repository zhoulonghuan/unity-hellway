﻿using UnityEngine;

namespace Kryz.CharacterStats.Examples {
  public class DeathZone : MonoBehaviour {
    [SerializeField] private Transform playerTransform = null;
    [SerializeField] private PlatformGenerator platformGenerator = null;

    private float deathZoneY;
    private Vector3 startingPlayerPos;

    private void Start () {
      deathZoneY = transform.position.y;
      startingPlayerPos = playerTransform.position;
    }

    private void Update () {
      Vector3 newPos = playerTransform.position;
      newPos.y = deathZoneY;
      transform.position = newPos;
    }

    private void OnTriggerEnter (Collider other) {
      if (other.transform == playerTransform) {
        playerTransform.position = startingPlayerPos;
        platformGenerator.Reset();
      }
    }
  }
}
