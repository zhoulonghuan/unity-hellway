item name
(description)

category
can shoot under water
under water damage reduction
has infinite ammo
compatible ammo types
has attachments
->attachments (another page)
caliber type
compatible caliber types
(if overheating is enabled) shots before overheating
(if malfunction is enabled) malfunction taps until unjammed
(if malfunction is enabled) malfunction percent failure to fire
(if malfunction is enabled) malfunction percent failure to eject
(if malfunction is enabled) malfunction percent failure to go into battery
(if malfunction is enabled) malfunction percent double feed
(if malfunction is enabled) malfunction percent defective round

has secondary fire

weapon type
(if weapon type is projectile) muzzle velocity
(if weapon type is projectile) projectile is thrown
(if burst fire is in firemodes list) burst fire delay
impact force
range
firemodes (as icons)

can regenerate ammo
renegate ammo per second
cost per shot
pellets per shot
magazine + max
reserve + max
ammo type

is chargeable
has kickback
has recoil
(if eject magazine is enabled) eject magazine time
reload type
reload only when empty
reload time tactical
reload time empty
has different rpm
(if has different rpm is disabled) rounds per minute
(if has different rpm is enabled) rounds per minute hold
(if has different rpm is enabled) rounds per minute tap
has spread
(more info about spread)
