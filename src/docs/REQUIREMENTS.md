TODO:s  
- Able to slot in their own damage type on the weapon?
- List of classes
- List of weapons available in the game (different types, not different stats...)


# Requirements
Requirements for a released Hellway game.

## XP
- Max level: 100
- Required XP to level up is increased by 1000XP and XP is reset to 0XP when leveling up. Required XP for level 1 is 1000XP, 2000XP for level 2, etc.
- Able to gain XP from quests, monsters.
- Players are able to boost their XP gain using talents.

## Classes

## Stats & Talents
Players start with 3 talent points (at level 1), then gain additional +3 talent points each level.  
Acquired gear also changes the stats.

### Basic stats / Resistances:
| Stats  | For each point | Starting value             | Type | Description |
| ------ | -------------- | -------------------------- | ---- | ----------- |
| Health | +10 HP         | (different for each class) | Flat | Every organic entity has health |
| Armor  | +5 Armor       | (different for each class) | Flat | 
| Shield | +5 Shield      | (different for each class) | Flat |

### Secondary stats:
| Stats | For each point | Starting value | Type | Description |
| ----- | -------------- | -------------- | ---- | ----------- |
| Stamina | +5 | 50 | Flat | Used when sprinting |
| Movement speed | 0.25% | 100% | Percent (unlimited) |
| Accuracy | 2.5% | 25% | Percent (limit 100%) |
| Critical hit chance | 2% | 10% | Percent (limit 100%) |
| Critical hit damage | 1% | 25% | Percent (unlimited) |
| Double damage % | 2.5% | 0% | Percent (limit 100%) |
| Deflect attacks % | 0.5% | 0% | Percent (limit 50%) |
| Explosive attack bonus | 2.5% | 0% | Percent (unlimited) |
| Melee attack bonus | 2.5% | 0% | Percent (unlimited) |
| Ranged attack bonus | 2.5% | 0% | Percent (unlimited) |
| XP gain bonus | 10% | 0% | Percent (unlimited) |
| Health regen | 1 | 0 | Flat |
| Health regen cooldown reduction | 0.15 | 10 | Flat |
| Armor regen | 2 | 0 | Flat |
| Armor regen cooldown reduction | 0.2 | 10 | Flat |
| Shield regen | 3 | 0 | Flat |
| Shield regen cooldown reduction | 0.25 | 10 | Flat |
| Weapon switch speed increase | 5% | 0% | Percent (unlimited)

### Weapon stats
| Stats | For each point | Starting value | Type |
| ----- | -------------- | -------------- | ---- |
| Reload speed reduction | 0.1% | 100% | Percent (limit 50%) |
| Recharge speed reduction (energy weapons) | 0.1% | 100% | Percent (limit 50%) |
| Rate of Fire increase | 0.1% | 100% | Percent (limit 150%) | 
| Projectile speed increase | 5% | 100% | Percent (limit 200%) |
| Projectile range increase | 5% | 0% | Percent (limit 100%) |
| Armor penetration bonus | 2.5% | 0% | Percent (limit 50%) |
| Shield penetration bonus | 2.5% | 0% | Percent (limit 50%) |
| Ammo capacity bonus: Magazine | +5 | (different for each weapon) | Flat (limit +25) |
| Ammo capacity bonus: Reserve | +10 | (different for each weapon) | Flat (limit +100) |
| Area of Effect bonus radius | 1 | 0 | Flat |
| Knockback force | +5 | 0 | Flat |
| Knockdown chance | 1.5% | 0% | Percent (limit 25%) |
| Physical damage bonus | 2.5% | 0% | Percent (unlimited) |
| Poison damage bonus | 2.5% | 0% | Percent (unlimited) |
| Energy damage bonus | 2.5% | 0% | Percent (unlimited) |
| Lightning damage bonus | 2.5% | 0% | Percent (unlimited) |
| Fire damage bonus | 2.5% | 0% | Percent (unlimited) |
| Corruption damage bonus | 2.5% | 0% | Percent (unlimited) |

## Damage types:
| Damage type | Strong vs     | Weak vs      |
| ----------- | ------------- | ------------ |
| Physical    | Health (100%) | Shield (50%) |
| Poison      | Health (125%) | Armor (50%)  |
| Energy      | Shield (125%) | Health (50%) |
| Lightning   | Shield (100%) | Armor (50%)  |
| Fire        | Armor (100%)  | Health (50%) |
| Corruption  | Armor (125%)  | Shield (50%) |

## Weapons


<!-- - General stats:
  - Health
  - Armor
  - Shield -->
<!-- - Secondary stats:
  - Stamina
  - Movement speed
  - Accuracy
  - Critical hit chance
  - Critical hit damage
  - Explosive attack bonus
  - Melee attack bonus
  - Ranged attack bonus
  - Double damage %
  - Deflect attacks % -->
<!-- - Damage types & Resistances:
  - Physical
  - Poison
  - Energy
  - Lightning
  - Fire
  - Corruption -->
<!-- - Defensive:
  - Health regen
  - Armor regen
  - Shield regen -->
<!-- - Weapons:
  - Reload speed
  - Recharge speed (energy weapons)
  - Rate of Fire
  - Projectile speed
  - Projectile range
  - Armor penetration bonus
  - Shield penetration bonus
  - Ammo capacity: Magazine
  - Ammo capacity: Reserve
  - Area of Effect -->



<!-- 
NOT TO BE USED
- Strength
- Agility
- Vitality
- Intelligence
- Magic
- Defense
- Water
- Nature
-->







<!-- 
- Attack
Pure and simple, how effective the weapon is in combat.
  - Power  
    Typical amount of damage the weapon inflicts when it hits the target.
  - Element  
    Whether or not the weapon uses Elemental Rock–Paper–Scissors when inflicting damage. Most weapons are Non-Elemental, but elemental weapons tend to have follow the same interactions as the local magic system.
  - Damage Type  
    A secondary Elemental Rock–Paper–Scissors designed around what kind of armor protects against what kind of weapon. E.g. Physical versus magical damage, slashing versus piercing attacks, and so on.
  - Additional Effects  
    Any secondary effects (often Standard Status Effects) that the weapon may impose on the target.
  - Armor Penetration  
    Some enemies are given a defense stat that reduces or negates damage, and being able to perform an Armor-Piercing Attack can be important.
  - Knockdown and Knockback  
    Some games give different weapons the ability to trip, stagger, or stumble foes.
- Area of Effect  
  How large an area the impact affects.
- Defense  
  Whether the weapon can be used to defend against enemy attacks.
- Parry/Counter Rate  
  Whether the weapon can be used to defend against an enemy's attacks, and/or allow the user to Counter Attack.
- Speed  
  How quickly the weapon's user can execute attacks with it. Often varies inversely with weight; in turn-based systems, lightweight weapons may strike multiple times or have higher Action Initiative than heavier weapons.
  - Rate of Fire  
    For ranged weapons, how many shots the weapon fires in a given period of time. Machine Guns tend to have high rates of fire while shotguns have lower ones, for example.
  - Reload Speed  
    How quickly the next projectile(s) is loaded into the weapon. A very common aspect in FPS games.
    - Recharge Rate  
      How quickly a weapon recharges. A common trait for Energy Weapons.
  - Combo Rate  
    How likely it is to deal multiple hits at once.
  - Switching Speed  
    How long it takes to switch to and/or from another weapon.
- Range  
  Maximum distance that can exist between user and opponent and still hit them with it. Applies to both long distance and melee weapons. Can sometimes affect damage or accuracy. Some weapons even have a minimum range within which they are very ineffective or useless, such as a grenade launcher needing to fly a certain distance before the warhead arms.
- Accuracy  
  Chances of successfully landing a hit with the weapon (primarily in turn-based systems). Sometimes varies inversely with damage or rate of fire.
  - Critical Hit Rate and Damage  
  The chance of landing a Critical Hit and how much of a difference that critical hit will make.
- Size  
  How much space a weapon will take in your inventory; may present an Inventory Management Puzzle, especially if combined with a Grid Inventory.
- Weight  
  How much a weapon affects your character's movement speed.
- Hands  
  Whether the weapon is wielded with one or two hands; controls whether the character is able to equip a shield (or a second weapon) in combination with it. Some weapons can even be restricted to the right or left hand only.
- Ammo Capacity  
  Also associated with ranged weapons, how many total attacks the user can make with it. Weapons with secondary fire modes may have ammo tracked separately for each mode, or may draw from the same pool of ammo at differing rates. Can be broken down into:
  - Magazine / Clip Capacity  
    Number of attacks possible before a reload is required for reloadable weapons, or for weapons with a recharge rate, the number of consecutive attacks possible before it is necessary to wait for the recharge. If the weapon recharges faster than it fires, the capacity is effectively infinite.
  - Reserve Capacity  
    Number of rounds of ammo or number of replacement clips that can be carried at one time. Commonly this is tracked excluding rounds in the clip, so that a weapon with an empty clip but full ammo reserve must be reloaded before further ammo can be collected. The use of the One Bullet Clips trope determines if partially consumed clips are tracked or returned to a general pool.
- Durability  
  How resistant the weapon is against wear and tear. Weapons may be indestructible, or may require regular maintenance to keep in working condition.
- Ammo Type  
  What type of ammunition a projectile weapon uses, mainly affecting whether or not different characters are allowed to share ammunition, or if different weapons draw from a common pool of ammo.
- Upgrades/Accessories  
  Whether the weapon's basic capabilities can be enhanced or customized by the user. Gun Accessories and magical upgrades are very common.
- Noise  
The volume of noise a weapon makes when used, which is important in stealth. Hollywood Silencers are used to dampen the sounds of firearms.
- Projectile Speed, Arc, and Visibility  
  For ranged attacks, some projectiles can be seen and dodged, and some are slow enough that the user must lead the target a bit. Other weapons are Hitscan. Some projectiles are affected by gravity, and some are not. Finally, whether the target can trace a missed or non-fatal shot back to the source.
- Skill  
  More common in RPG's, the types of characters that can equip the weapon and the skills a character needs to use the weapon to full effect.
-->



