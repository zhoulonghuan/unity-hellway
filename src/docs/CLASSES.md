# Classes
This is a more descriptive information about the classes available in [Hellway](https://gitlab.com/Saucyminator/unity-hellway).
All classes have male and female counterparts.

## Overview
| Class name | HP/Armor/Shield                | Uses armor | Uses shield | Weapon | Sidearm | Utility | Buffs |
| ---------- | :----------------------------: | :--------: | :---------: | ------ | ------- | ----- | --- |
| Assault    | 120 ❤️ <br> 25 🛡️ <br> 25 🔋   | ✔️️️️️         | ✔️️          | Assault rifles, Shotguns | Pistols, Melee       | Grenades    | 10% more HP |
| Demolition | 100 ❤️ <br> 20 🛡️              | ✔️️         | ❌          | RPGs, Grenade launchers | Pistols, SMGs, Melee  | Grenades    | 10% more explosive damage |
| Engineer   | 80 ❤️ <br> 20 🛡️               | ✔️️         | ❌          | SMGs, Shotguns           | Pistols, Melee        | Armor pack  | 10% more armor |
| Medic      | 100 ❤️ <br>        <br> 40 🔋  | ❌         | ✔️️          | SMGs, Shotguns           | Pistols, Melee       | Med pack    | 10% HP regen |
| Pyromaniac | 80 ❤️ <br> 20 🛡️ <br> 40 🔋    | ✔️️         | ✔️️          | Flamethrowers, Shotguns  | Pistols, SMGs, Melee | Grenades    | 10% fire resistance |️
| Marksman   | 80 ❤️                          | ❌         | ❌          | Bows, Sniper rifles     | Pistols, Melee        | Grenades    | 10% ranged attack bonus |
| Support    | 80 ❤️ <br> 30 🛡️               | ✔️️         | ❌          | LMGs                    | Pistols, Melee        | Ammo pack   | 10% more ammo |

## Classes in depth
### Assault
A class that can take a beating.

### Demolition
Explosives!

### Engineer
Can build things that kill things.

### Medic
Can heal teammates.

### Pyromaniac
Let there be light!

### Marksman
Pew pew from far away.

### Support
Can resupply teammates with ammo.


## Utility

## Buffs
