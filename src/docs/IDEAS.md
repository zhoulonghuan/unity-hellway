NOTE: It's going to a whole lot easier when v2018.3 is released (nested prefabs, etc)
- Base weapon with transforms for FirePoint, CasingEjectin, Interact, etc

Requirements:
- Support different types of weapons:
  - Rifles (Combat, Assault)
  - Shotguns
  - Snipers
  - Pistols
  - Crowbar
  - Bow
  - Rocket launcher
  - Grenade launcher
  - Throwables (Grenades, Molotov, etc)
  - Energy weapons (needs charge-up)
- Support attachments (Laser sight, Flashlight, Magazine, Accessory, etc)
- Each type of weapons should be able to use any type of ammo (bow with grenades, rifle with arrows, etc)
- ? Modular/Independant, should not require stuff like Config (or should it?)
- Should implement interfaces depending on their item
  - Two different SMGs can have different setups. One can have IAttachments while the other has not.
- ScriptableObject to store the data (like damage, range, etc) without it being on the prefab or whatnot
  - ? Should each interface/component have a separate ScriptableObject for just that component?
- ? All weapons should be on the Player/Entity GameObject/prefab and register at a RuntimeSet?
- Testable
- Support inventory UI. Weapons should be items that can be picked up and added to the UI if needed.

TODO:s
- Should I do projectiles or hitscan? DECIDE!
- How to handle transforms of the weapon (attachments, effects, etc)? Should all weapons have all available attachments even if it doesn't use it?
  - Should each script check if if transform is available on the weapon (something like WEAPON/Transforms/<the transform>?
- Weapon data. Example: SMG is an abstract scriptableobject

Optional:
- When talking to friendlies/in friendlies zone, have the weapons at a idle/angle so not to aim straight to the person.

Visual effects:
- Film grain when near radiation visual effect [Black Mesa]
