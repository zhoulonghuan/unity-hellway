Alt (each lvl up starts from 0 xp):
Level:  1 Exp:  1000 Diff:  1000 Total: 1000
Level:  2 Exp:  2000 Diff:  2000 Total: 3000
Level:  3 Exp:  3000 Diff:  3000 Total: 6000
Level:  4 Exp:  4000 Diff:  4000 Total: 10000
Level:  5 Exp:  5000 Diff:  5000 Total: 15000
Level:  6 Exp:  6000 Diff:  6000 Total: 21000
Level:  7 Exp:  7000 Diff:  7000 Total: 28000
Level:  8 Exp:  8000 Diff:  8000 Total: 36000
Level:  9 Exp:  9000 Diff:  9000 Total: 45000
Level: 10 Exp: 10000 Diff: 10000 Total: 55000
Total: 55000

WIP
NoAlt (each lvl up starts from previous xp):
Level:  1 Exp:  1000 Diff:  1000 Total: 1000
Level:  2 Exp:  3000 Diff:  2000 Total: 3000
Level:  3 Exp:  6000 Diff:  3000 Total: 6000
Level:  4 Exp: 10000 Diff:  4000 Total: 10000
Level:  5 Exp: 15000 Diff:  5000 Total: 15000
Level:  6 Exp: 21000 Diff:  6000 Total: 21000
Level:  7 Exp: 28000 Diff:  7000 Total: 28000
Level:  8 Exp: 36000 Diff:  8000 Total: 36000
Level:  9 Exp: 45000 Diff:  9000 Total: 45000
Level: 10 Exp: 55000 Diff: 10000 Total: 55000
Total: 55000
