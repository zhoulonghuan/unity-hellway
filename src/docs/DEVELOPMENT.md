# Development

## Git commit messages (what they basically mean)
- **Added**
  - Any type of addition to the game.
- **Updated**
  - Non-balance updates, updated values of something that I had forgotten, etc. Also used for when updating plugins.
- **Changed**
  - Tweaking settings for balance.
- **Fixed**
  - Bugfixes.
- **Removed**
  - Removal from the game.
- **Cleanup**
  - Cleanup of code or files which should've been modified when that commit was made. Variable renames, add/remove notes in code. Etc, etc.

## Git branches
- **Master**
  - Stable game where you could compile the game yourself and run it.
  - _**Note**: Once I get to a point where the game is stable (currently working on v0.3.0 to be stable) then I'll move to the **Dev**-branch._
- **Dev**
  - Development of features, bugfixes, etc.

## Asset folders (located in /Assets)
_**NOTE**: This is going to be updated/reorganized very often so this list can be inaccurate._
- **/Animations**
  - Animations that are not character model related.
- **/Audio**
  - Sound files. Music, SFX, etc, files. Mp3 files and whatnot.
- **/Fonts**
  - Font files.
  - Also contains TextMeshPro files it needs to be able to display the font.
- **/Materials**
- **/Models**
  - Character, environment, weapon, etc models.
  - Also contains animations related to the model.
- **/Plugins**
- **/PostProcessing**
- **/Prefabs**
- **/Presets**
  - Unity's "Presets". Basically it saves component information that can be used over and over again, like a template or similar.
- **/Resources**
- **/Scenes**
- **/Scripts**
- **/Sounds**
  - ScriptableObjects that has files from **/Audio** but makes it easier to use in the project.
- **/Standard** Assets
  - Unity's "Standard Assets". Maybe shouldn't be included but it's been there from the start.
- **/Textures**
  - Image/texture files.
