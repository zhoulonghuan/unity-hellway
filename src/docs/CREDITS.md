# Credits
## Code
- Sebastian Lague:
  - This game uses some modified code from [Sebastian Lague](https://www.youtube.com/user/Cercopithecan)'s ["Create a Game" tutorial series](https://youtu.be/SviIeTt2_Lc?list=PLFt_AvWsXl0ctd4dgE1F8g3uec4zKNRV0) ([source code for his project](https://github.com/SebLague/Create-a-Game-Project)).
- Brackeys:
  - ~~This game also uses modified code from [Brackeys](https://www.youtube.com/user/Brackeys)' inventory and health code. Which is from Brackeys/Sebastian Lague's [RPG tutorial series](https://www.youtube.com/playlist?list=PLPV2KyIb3jR4KLGCCAciWQ5qHudKtYeP7) ([source code for their project](https://github.com/Brackeys/RPG-Tutorial)).~~ Not anymore.

## Audio
- [UI Audio by Kenney](http://kenney.nl/assets/ui-audio)

## Icons
- [Game Icons](http://game-icons.net/)

## Models:
- [Vepr VPO 205-00 by yarogor](https://sketchfab.com/models/cbcafadc46c24b10b1949fbc01d93b4a)
- [Colt Python by minhdaow](https://sketchfab.com/models/c0e32342bd344610bc2f614ef8f89fc0)
- [Low-Poly AK12 by TastyTony [Low-poly]](https://sketchfab.com/models/cb9a1be014b942168c745268f0af2560)
- [Sig Sauer P226 by Yksnawel](https://sketchfab.com/models/53072553551e403a921448ee5581990e)
- [Mossberg 500 Pump Shotgun by Samurai_Chad](https://sketchfab.com/models/9899f3a9f5c948a9b1e89470f0dfc132)
- [Silencer by shaz_snow](https://sketchfab.com/models/5a6e0052b06b4b109eec6e3d6002e6ed)
- [Silencer556 by MyNameIsVoo](https://sketchfab.com/models/b7a1da621c1f4270aa838633087a4ae6)
- [DEAGLE by AvnisT](https://sketchfab.com/models/ff635600c2ac48ba9557bb06f9f682e9)
- [M1- Garand Sp RH by debarjun.das](https://sketchfab.com/models/1b74d85199ff4125abe173a34ebb5ce6)
- [UMP-45 by 7XF Design (Modern Weapons Pack)](https://assetstore.unity.com/packages/3d/props/guns/modern-weapons-pack-14233)
- [Basic Double Barrel by ChristopherBates [the "Uplander Supreme"]](https://sketchfab.com/models/1b3efa9c56274d68bf17fdb38a742388)
- [SPAS 12 Gauge Shotgun by Visual Lucidity](https://sketchfab.com/models/e6b78a3feb644206b4ea79bee6d7f086)
- [Lowpoly RPG-7 by veti](https://www.blendswap.com/blends/view/25166)
- [M134 Minigun by Pieter Ferreira](https://sketchfab.com/models/a1571536d6a641eeaf30aed62c7c8d34)
- [Milkor MGL 140 by BrutalTronics](https://gamebanana.com/models/1503)
- [Bow by daniel_slusarz [Low-poly]](https://sketchfab.com/models/106666ac28c84528b6a616f92b859437)
- [Modern weapons by Matias Hollmann [AK-47, M4 Carbine, L96A1]](http://devassets.com/assets/modern-weapons/)
- [StreetWars: Beretta by Zuhian Teiyu](https://sketchfab.com/models/7ed772ef54314b04896a4d3a126967b9)
- [M249 Machine Gun by Buce](https://sketchfab.com/models/bf0105376a2d4c37926f3ce95316460d)
- [STG-44 by Arbuzz747](https://sketchfab.com/models/fe139cf77b384d3fbf40bef53a6c809c)
- [HK MP5 (dimensional prototype) by Yksnawel](https://sketchfab.com/models/f7826d683c0a4d46adcfb4e9c4e83f6f)
