# Hellway
Hellway is a Top-Down shooter that takes place in space, fighting off swarms of demons. Choose between different classes which has unique talents and weapons associated with them. The project is currently in pre-alpha.

Currently working on a complex weapon system that can support the many things I'd like to have in the game (and future games).

![Hellway](src/images/Hellway_v0.2.x.png)

## Information
This project is semi-active. Meaning I tend to work on it when I feel like it. I'm usually busy playing games rather than programming. Also, I tend not to do many commits often and do them in bulk instead (trying to get better at committing more often).

I work slow because of me being a perfectionist and before committing I tend to make sure things actually work.

## Planned features:
- Classes: Choose between different classes that excel at different things ([Classes more in-depth](src/docs/CLASSES.md))
- CO-OP multiplayer game (focusing on single-player for now, coding a multiplayer is hard)
- Complex weapon system

See more here: [FEATURES](src/docs/FEATURES.md)

## Development
Things related to development and more info for developers can be found here: [DEVELOPMENT info](src/docs/DEVELOPMENT.md)

## Credits
[See CREDITS](src/docs/CREDITS.md)

## Changelog
[See CHANGELOG](CHANGELOG.md)

## License
[MIT License](LICENSE.md)
